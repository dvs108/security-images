# Hadolint

A smarter Dockerfile linter that helps you build best practice Docker images.

https://github.com/hadolint/hadolint