# Security Images for Pipelines


## Secret scanners
- Gitleaks
- Trufflehog3 (fork from trufflehog for dojo)

## Code scanners
- bandit
- brakeman
- eslint
- gosec
- retirejs
- semgrep
- sonarqube
- hadolint
- gixy

## Code dependency scanners
- Trivy

## Image dependency scanners
- Trivy
- Grype

## Dynamic scanners
- Arachni

## Infrastructure scanners
- Subfinder
- Nuclei

